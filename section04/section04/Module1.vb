﻿Imports System.IO

Module Module1

    Sub Main()
        Dim file As IO.FileStream = New IO.FileStream("File.txt", FileMode.OpenOrCreate, FileAccess.ReadWrite)
        Dim itr As Integer


        For itr = 0 To file.Length
            Console.WriteLine(file.ReadByte)
        Next

        ' Writing code
        ' For itr = 0 To 100
        '   file.WriteByte(CByte(itr))
        ' Next


        file.Close()
    End Sub

End Module
