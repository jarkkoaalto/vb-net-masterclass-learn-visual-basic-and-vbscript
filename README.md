# VB-NET-Masterclass-Learn-Visual-Basic-and-VBScript

Learn about everything there is to know about Visual Basic Applications and How To Program Them. A step by step process is used to show explain every facet of these topics.

Course content

###### Section 01 and 02: Basics
###### Section 03: Classes
###### Section 04: File Handling
###### Section 05: GUI
###### Section 06: Dialog Box
###### Section 07: Advanced
###### Section 08: Extra Learning Resources