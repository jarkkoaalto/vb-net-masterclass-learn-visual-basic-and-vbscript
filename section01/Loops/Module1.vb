﻿Module Module1

    Sub Main()
        Dim i As Integer
        i = 0
        ' For loop
        Console.WriteLine("For Loop")
        For i = 0 To 10 Step 1
            Console.WriteLine("Kaboom " & i)
        Next
        Console.WriteLine("")

        ' While loop
        i = 0
        Console.WriteLine("While Loop")
        While i <= 10
            Console.WriteLine("Zap " & i)
            i = i + 1 'i += 1
        End While
        Console.WriteLine("")

        ' Do While loop
        Console.WriteLine("Do While loop")
        i = 0
        Do
            Console.WriteLine("Boom " & i)
            i = i+1
        Loop While (i <= 10)





    End Sub

End Module
