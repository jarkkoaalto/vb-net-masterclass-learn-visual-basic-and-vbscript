﻿Module Module1

    Sub PackSub()
        Console.WriteLine("Hi there welcome back")
    End Sub

    Sub AddSub(ByVal numA As Integer, ByVal numB As Integer)
        Dim result As Integer
        result = numA + numB
        Console.WriteLine(result)
    End Sub
    ' Ref sub co to the memory location and override this location and write 200 instead
    Sub RefSub(ByRef num1 As Integer)
        num1 = 200
    End Sub

    Sub ValSub(ByVal num1 As Integer)
        num1 = 16
    End Sub


    Sub Main()
        PackSub()
        PackSub()

        AddSub(10, 56)

        Dim i As Double
        Dim j As Double

        i = 7.12
        j = 7.12

        ValSub(i)
        RefSub(j)
        Console.WriteLine(i)
        Console.WriteLine(j)
    End Sub

End Module
