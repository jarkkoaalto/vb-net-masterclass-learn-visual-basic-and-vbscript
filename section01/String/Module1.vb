﻿Module Module1

    Sub Main()
        Dim str As String
        Dim str2 As String

        str = "Hello"
        str2 = "World"

        Console.WriteLine(str)
        ' concatenation
        Dim newStr = str + " " + str2
        Console.WriteLine(newStr)

        Dim Len As Integer
        Len = newStr.Length
        Console.WriteLine(Len)

        Console.WriteLine(newStr.ToUpper())
    End Sub

End Module
