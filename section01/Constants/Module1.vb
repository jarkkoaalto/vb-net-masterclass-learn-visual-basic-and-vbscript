﻿Module Module1

    Sub Main()
        Const VarAwesom As String = "This is Awesome"
        Dim Vae2 As Integer
        Vae2 = 30

        Const Number As Integer = 5

        Console.WriteLine(VarAwesom)
        Console.WriteLine(Number)
        Console.WriteLine(Vae2)

        ' HUOMIO
        ' Constant cannot be the target of an assignment
        ' Number = Vae2


        Console.WriteLine("Sum is : " & Number + Vae2)

    End Sub

End Module
