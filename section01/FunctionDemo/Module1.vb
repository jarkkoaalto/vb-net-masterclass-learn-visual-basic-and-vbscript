﻿Module Module1

    Function AddFun(ByVal num1 As Integer, ByVal num2 As Integer) As Integer
        Dim result As Integer
        result = num1 + num2

        AddFun = result
    End Function

    Sub Main()

        Dim var1 As Integer
        ' Function can also add variable
        var1 = AddFun(8, 8)
        Console.WriteLine(AddFun(5, 6))
        Console.WriteLine(var1)

    End Sub

End Module
