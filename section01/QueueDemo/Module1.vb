﻿Module Module1

    Sub Main()
        Dim x As Queue = New Queue()

        Dim stk As String
        Dim str As String

        x.Enqueue("One")
        x.Enqueue("Two")
        x.Enqueue("Three")
        x.Enqueue("Four")
        x.Enqueue("Five")

        For Each str In x
            Console.WriteLine(str)
        Next

        x.Dequeue()
        x.Dequeue()
        x.Dequeue()

        Console.WriteLine()
        Console.WriteLine("After popping")
    End Sub

End Module
