﻿Module Module1

    Sub Main()
        'Arithmetic oreators
        Console.WriteLine(5 / 9)
        Console.WriteLine(10 + 4)

        Console.WriteLine(5 * 5)
        Console.WriteLine(5 - 5)

        ' Comaparison operators
        Console.WriteLine(5 = 5)
        Console.WriteLine(5 = 5)
        Console.WriteLine(5 = 8)

        ' Logical/Bitwise operators
        Console.WriteLine(5 And 5)
        Console.WriteLine(5 Or 8)

        ' Assigment operators
        Dim var1 As Integer = 9
        Console.WriteLine(var1)
        var1 += 10
        Console.WriteLine(var1)

    End Sub

End Module
