﻿Module Module1

    Sub Main()
        Console.WriteLine("Enter Number (1 to 3)")
        Dim input As Integer
        input = Console.ReadLine()

        Select Case input
            Case 1
                Console.WriteLine("Menu option 1 selected")
            Case 2
                Console.WriteLine("Menu option 2 selected")
            Case 3
                Console.WriteLine("Menu option 3 selected")
            Case Else
                Console.WriteLine("Invalid option")
        End Select

    End Sub

End Module
