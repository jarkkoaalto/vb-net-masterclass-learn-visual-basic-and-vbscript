﻿Module Module1

    Sub Main()
        Dim IntVar As Integer
        IntVar = 100

        Dim LongVar As Long
        LongVar = 203004040534

        Dim DoubleVar As Double
        DoubleVar = 2.004056

        Dim StringVar As String
        StringVar = "Everything is Bossible"

        Dim CharVar As Char
        CharVar = "A"


        Dim BooleanVar As Boolean
        BooleanVar = True


        Console.WriteLine("Integer : " & IntVar)
        Console.WriteLine("Long : " & LongVar)
        Console.WriteLine("Double : " & DoubleVar)
        Console.WriteLine("String : " & StringVar)
        Console.WriteLine("Char : " & CharVar)
        Console.WriteLine("Boolean : " & BooleanVar)

    End Sub

End Module
